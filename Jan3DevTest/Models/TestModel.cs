﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Jan3DevTest.Models
{
    public class TestModel
    {
        public int ID { get; set; }
        public string Font { get; set; }
        public string Color { get; set; }
        public int Size { get; set; }

          [DataType(DataType.DateTime, ErrorMessage = "Please enter a valid date in the format dd/mm/yyyy hh:mm")]
        public DateTime Timestamp { get; set; }

        public string Text { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Location { get; set; }
    }
}