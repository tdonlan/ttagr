﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Jan3DevTest.Models
{
    public class ttagrDataModel : IEquatable<ttagrDataModel>, IComparable<ttagrDataModel>
    {
        public int ID { get; set; }
        public string Font { get; set; }
        public string Color { get; set; }
        public int Size { get; set; }

        [DataType(DataType.DateTime, ErrorMessage = "Please enter a valid date in the format dd/mm/yyyy hh:mm")]
        public DateTime Timestamp { get; set; }

         [Display(Name = "Tag")]
         [StringLength(50, MinimumLength = 3)]
        public string Text { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Location { get; set; }

        public ttagrDataModel()
        { 
        }

        public ttagrDataModel(string text, double lat, double lon)
        {
            this.Text = Text;
            this.Timestamp = DateTime.Now;
            this.Latitude = lat;
            this.Longitude = lon;

        }


        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            ttagrDataModel objAsPart = obj as ttagrDataModel;
            if (objAsPart == null) return false;
            else return Equals(objAsPart);
        }
        public int SortByNameAscending(string name1, string name2)
        {

            return name1.CompareTo(name2);
        }

        // Default comparer for Part type. 
        public int CompareTo(ttagrDataModel comparePart)
        {
            // A null value means that this object is greater. 
            if (comparePart == null)
                return 1;

            else
            {
                return (int)DistanceHelper.getDistance(this.Latitude, this.Longitude, comparePart.Latitude, comparePart.Longitude);
               
            }
               
        }

        public override int GetHashCode()
        {
            return ID;
        }

        public bool Equals(ttagrDataModel other)
        {
            if (other == null) return false;
            return (this.ID.Equals(other.ID));
        }
    }
}