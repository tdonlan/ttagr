﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Jan3DevTest.Startup))]
namespace Jan3DevTest
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
