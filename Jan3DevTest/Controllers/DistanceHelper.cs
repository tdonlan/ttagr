﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Jan3DevTest
{
    public class DistanceHelper
    {

        //From: http://www.movable-type.co.uk/scripts/latlong.html

        /*
         * var R = 6371; // km
            var dLat = (lat2-lat1).toRad();
            var dLon = (lon2-lon1).toRad();
            var lat1 = lat1.toRad();
            var lat2 = lat2.toRad();

            var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                    Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
            var d = R * c;
         * */
        public static double getDistance(double lata, double lona, double latb, double lonb)
        {
            double R = 6371; //km 
            double dLat = toRad(latb - lata);
            double dLon = toRad(lonb - lona);
            lata = toRad(lata);
            latb= toRad(latb);

            double a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
                    Math.Sin(dLon / 2) * Math.Sin(dLon / 2) * Math.Cos(lata) * Math.Cos(latb);

            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));

            double distance = R * c;

            return distance;
        }

        public static double toRad(double val)
        {
            return val * Math.PI / 180;
        }
    }
}