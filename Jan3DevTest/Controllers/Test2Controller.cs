﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Jan3DevTest.Models;

namespace Jan3DevTest.Controllers
{
    public class Test2Controller : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: /Test2/
        public ActionResult Index()
        {
            return View(db.TestModels.ToList());
        }

        public ActionResult DisplayWordCloud()
        {
            return View(db.TestModels.ToList());
        }

        // GET: /Test2/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TestModel testmodel = db.TestModels.Find(id);
            if (testmodel == null)
            {
                return HttpNotFound();
            }
            return View(testmodel);
        }

        // GET: /Test2/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Test2/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,Text,Location")] TestModel testmodel)
        {
            if (ModelState.IsValid)
            {

                testmodel.Timestamp = DateTime.Now;

                db.TestModels.Add(testmodel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(testmodel);
        }

        // GET: /Test2/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TestModel testmodel = db.TestModels.Find(id);
            if (testmodel == null)
            {
                return HttpNotFound();
            }
            return View(testmodel);
        }

        // POST: /Test2/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,Text,Location")] TestModel testmodel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(testmodel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(testmodel);
        }

        // GET: /Test2/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TestModel testmodel = db.TestModels.Find(id);
            if (testmodel == null)
            {
                return HttpNotFound();
            }
            return View(testmodel);
        }

        // POST: /Test2/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TestModel testmodel = db.TestModels.Find(id);
            db.TestModels.Remove(testmodel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

  
        public ActionResult ViewLocation()
        {
            int id = 2;

            TestModel testmodel = db.TestModels.Find(id);
            if (testmodel == null)
            {
                return HttpNotFound();
            }
            return View(testmodel);
            
        }
    }
}
