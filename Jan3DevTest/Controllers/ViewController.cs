﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Jan3DevTest.Models;
using System.Data.Entity.Core.Objects.DataClasses;

namespace Jan3DevTest.Controllers
{
    public class ViewController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: /View/
        public ActionResult Index()
        {
            return View(db.ttagrDataModels.ToList());
        }

        [HttpPost]
        public JsonResult ViewDataPost(double lat, double lon)
        {

            List<ttagrDataModel> tempList = new List<ttagrDataModel>();

            var tags = from t in db.ttagrDataModels
                       where Math.Abs(t.Latitude - lat) < 0.1 && Math.Abs(t.Longitude - lon) < 0.1
                       select t;

            tempList.AddRange(tags.ToList());
               
            ttagrDataModel tempModel = new ttagrDataModel("99999",lat,lon);
            tempList.Add(tempModel);
            tempList.Sort();
            tempList.Remove(tempModel);
            tempList = tempList.OrderByDescending(a => a.Timestamp).ToList();
            
            return Json(tempList.Take(10));
        }

        public ActionResult ViewData()
        {
            return View();
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
   
        public ActionResult ViewGrafitti()
        {
            return View();
        }
    
    }
}
