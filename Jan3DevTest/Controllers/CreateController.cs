﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Jan3DevTest.Models;

namespace Jan3DevTest.Controllers
{
    public class CreateController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: /Create/
        public ActionResult Index()
        {
            return View(db.ttagrDataModels.ToList());
        }

        // GET: /Create/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ttagrDataModel ttagrdatamodel = db.ttagrDataModels.Find(id);
            if (ttagrdatamodel == null)
            {
                return HttpNotFound();
            }
            return View(ttagrdatamodel);
        }

        // GET: /Create/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Create/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,Font,Color,Size,Timestamp,Text,Latitude,Longitude,Location")] ttagrDataModel ttagrdatamodel)
        {

            ttagrdatamodel.Timestamp = DateTime.Now;
            ttagrdatamodel.Size = 10;
            ttagrdatamodel.Font = "Courier";
            ttagrdatamodel.Color = "Black";

            if (ModelState.IsValid)
            {
                db.ttagrDataModels.Add(ttagrdatamodel);
                db.SaveChanges();
                return RedirectToAction("Index","View");
            }

            return View(ttagrdatamodel);
        }

        // GET: /Create/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ttagrDataModel ttagrdatamodel = db.ttagrDataModels.Find(id);
            if (ttagrdatamodel == null)
            {
                return HttpNotFound();
            }
            return View(ttagrdatamodel);
        }

        // POST: /Create/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,Font,Color,Size,Timestamp,Text,Latitude,Longitude,Location")] ttagrDataModel ttagrdatamodel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ttagrdatamodel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(ttagrdatamodel);
        }

        // GET: /Create/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ttagrDataModel ttagrdatamodel = db.ttagrDataModels.Find(id);
            if (ttagrdatamodel == null)
            {
                return HttpNotFound();
            }
            return View(ttagrdatamodel);
        }

        // POST: /Create/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ttagrDataModel ttagrdatamodel = db.ttagrDataModels.Find(id);
            db.ttagrDataModels.Remove(ttagrdatamodel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
