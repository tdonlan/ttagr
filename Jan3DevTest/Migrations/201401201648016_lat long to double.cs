namespace Jan3DevTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class latlongtodouble : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ttagrDataModels", "Latitude", c => c.Double(nullable: false));
            AlterColumn("dbo.ttagrDataModels", "Longitude", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ttagrDataModels", "Longitude", c => c.String());
            AlterColumn("dbo.ttagrDataModels", "Latitude", c => c.String());
        }
    }
}
