namespace Jan3DevTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ttagrdatamodel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ttagrDataModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Font = c.String(),
                        Color = c.String(),
                        Size = c.Int(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        Text = c.String(),
                        Latitude = c.String(),
                        Longitude = c.String(),
                        Location = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ttagrDataModels");
        }
    }
}
