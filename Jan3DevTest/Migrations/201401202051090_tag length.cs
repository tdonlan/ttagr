namespace Jan3DevTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class taglength : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ttagrDataModels", "Text", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ttagrDataModels", "Text", c => c.String());
        }
    }
}
