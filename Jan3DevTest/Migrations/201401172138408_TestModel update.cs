namespace Jan3DevTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TestModelupdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TestModels", "font", c => c.String());
            AddColumn("dbo.TestModels", "color", c => c.String());
            AddColumn("dbo.TestModels", "size", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TestModels", "size");
            DropColumn("dbo.TestModels", "color");
            DropColumn("dbo.TestModels", "font");
        }
    }
}
