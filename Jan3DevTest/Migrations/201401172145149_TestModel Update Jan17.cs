namespace Jan3DevTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TestModelUpdateJan17 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TestModels", "Timestamp", c => c.DateTime(nullable: false));
            AddColumn("dbo.TestModels", "Latitude", c => c.String());
            AddColumn("dbo.TestModels", "Longitude", c => c.String());
            AlterColumn("dbo.TestModels", "Font", c => c.String());
            AlterColumn("dbo.TestModels", "Color", c => c.String());
            AlterColumn("dbo.TestModels", "Size", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TestModels", "Size", c => c.Int(nullable: false));
            AlterColumn("dbo.TestModels", "Color", c => c.String());
            AlterColumn("dbo.TestModels", "Font", c => c.String());
            DropColumn("dbo.TestModels", "Longitude");
            DropColumn("dbo.TestModels", "Latitude");
            DropColumn("dbo.TestModels", "Timestamp");
        }
    }
}
